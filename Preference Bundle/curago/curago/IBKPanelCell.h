//
//  IBKPanelCell.h
//  curago
//
//  Created by Matt Clarke on 22/02/2015.
//
//

#import <Preferences/Preferences.h>
#import "IBKPanelViewController.h"

@interface IBKPanelCell : PSTableCell

@property (nonatomic, strong) IBKPanelViewController *contr;

@end
